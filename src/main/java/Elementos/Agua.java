package Elementos;

public class Agua extends Elemento {

	String elementoQueMeHaceDaño = "Aire";
	String elementoQueHagoDaño = "Fuego";
	String nombre;

	public Agua() {
		nombre = "Agua";

	}

	public String nombre() {
		return nombre;
	}

	public String debilidad() {

		return elementoQueMeHaceDaño;

	}

	public String fuerteA() {

		return elementoQueHagoDaño;
	}

}
