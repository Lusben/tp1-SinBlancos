package Elementos;

public class Fuego extends Elemento {

	Fuego fuego;
	String elementoQueMeHaceDaño = "Agua";
	String elementoQueHagoDaño = "Tierra";
	String nombre;

	public Fuego() {
		nombre = "Fuego";

	}

	public String nombre() {
		return nombre;
	}

	public String debilidad() {

		return elementoQueMeHaceDaño;

	}

	public String fuerteA() {

		return elementoQueHagoDaño;

	}
}
