package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import Elementos.Agua;
import Elementos.Aire;
import Elementos.Elemento;
import Elementos.Fuego;
import Elementos.Tierra;
import Juego.Monstruo;

class MonstruoTest {

	@Test
	void SeCreaUnMonstruoConElementos() {
		Elemento agua = new Agua();
		Elemento tierra = new Tierra();

		Monstruo m1 = new Monstruo(agua, tierra);

		Assert.assertEquals(m1.elemento1, agua);
		Assert.assertEquals(m1.elemento2, tierra);

	}

	@Test
	void SeCreaUnMonstruoConElementosIguales() {
		Elemento agua = new Agua();
		Elemento agua2 = new Agua();

		Monstruo m1 = new Monstruo(agua, agua2);

		Assert.assertEquals(m1.elemento1, agua);
		Assert.assertEquals(m1.elemento2, agua2);
	}

	@Test
	void SeCreanDosMonstruoConElementos() {
		Elemento agua = new Agua();
		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		Elemento aire = new Aire();

		Monstruo m2 = new Monstruo(fuego, tierra);
		Monstruo m1 = new Monstruo(agua, aire);

		Assert.assertEquals(m1.elemento1, agua);

		Assert.assertEquals(m1.elemento2, aire);

		Assert.assertEquals(m2.elemento1, fuego);

		Assert.assertEquals(m2.elemento2, tierra);

	}

	@Test
	@DisplayName("Check the expectThrows")

	void SeCreanDosMonstruosConElementosInversos() throws Exception {
		String message = "El assert Fallara";

		Elemento agua = new Agua();
		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		Elemento aire = new Aire();

		Monstruo m1 = new Monstruo(agua, aire);
		Monstruo m2 = new Monstruo(fuego, tierra);

		assertThrows(IllegalArgumentException.class, () -> {

			throw new IllegalArgumentException(message);
		});
		
		assertEquals(m1.elemento1, tierra);

		assertEquals(m1.elemento2, fuego);

		assertEquals(m2.elemento1, agua);

		assertEquals(m2.elemento2, aire);
	}

	@Test
	@DisplayName("Check the expectThrows")
	void checkExceptions() {

		

		String message = "Paradigma rules!";

		Throwable exception = assertThrows(IllegalArgumentException.class, () -> {

			throw new IllegalArgumentException(message);

		});

		assertEquals(message, exception.getMessage());

	}
}
