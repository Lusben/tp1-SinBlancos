package Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Elementos.Agua;
import Elementos.Aire;
import Elementos.Elemento;
import Elementos.Fuego;
import Elementos.Tierra;
import Juego.Jugador;

class JugadorTest {

	@Test
	void SeCreaUnJugadorConMonstruos() throws Exception {

		Jugador j1 = new Jugador("Gian");

		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		j1.crearMonstruo(fuego, tierra);

		assertEquals(j1.miMonstruo.elemento1, fuego);
		assertEquals(j1.miMonstruo.elemento2, tierra);

	}

	@Test
	void SeCreanDosJugadorConMonstruos() throws Exception {

		Jugador j1 = new Jugador("Gian");

		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		j1.crearMonstruo(fuego, tierra);
		Jugador j2 = new Jugador("Luis");

		Elemento aire = new Aire();
		Elemento agua = new Agua();
		j2.crearMonstruo(aire, agua);

		assertEquals(j1.miMonstruo.elemento1, fuego);
		assertEquals(j1.miMonstruo.elemento2, tierra);

		assertEquals(j2.miMonstruo.elemento1, aire);
		assertEquals(j2.miMonstruo.elemento2, agua);

	}

	@Test
	void SeCreanDosMounstrosDeIgualElementos() throws Exception {

		Jugador j1 = new Jugador("Gian");

		Elemento fuego = new Fuego();

		j1.crearMonstruo(fuego, fuego);
		Jugador j2 = new Jugador("Luis");

		Elemento agua = new Agua();
		j2.crearMonstruo(agua, agua);

		assertEquals(j1.miMonstruo.elemento1, fuego);
		assertEquals(j1.miMonstruo.elemento2, fuego);

		assertEquals(j2.miMonstruo.elemento1, agua);
		assertEquals(j2.miMonstruo.elemento2, agua);

	}

	@Test
	void SeCreanDosJugadoresConElMismoElementoYMismoNombre() throws Exception {

		Jugador j1 = new Jugador("Luis");

		Elemento fuego = new Fuego();

		j1.crearMonstruo(fuego, fuego);
		Jugador j2 = new Jugador("Luis");

		Elemento agua = new Agua();
		j2.crearMonstruo(agua, agua);

		assertEquals(j1.miMonstruo.elemento1, fuego);
		assertEquals(j1.miMonstruo.elemento2, fuego);

		assertEquals(j2.miMonstruo.elemento1, agua);
		assertEquals(j2.miMonstruo.elemento2, agua);

		assertEquals(j1.obtenerNombre(), "Luis");

		assertEquals(j2.obtenerNombre(), "Luis");

	}

	@Test
	void SeCreanDosJugadoresConElMismoElementoYMismoNombreYLaVidaDeCadaUno() throws Exception {

		Jugador j1 = new Jugador("Luis");

		Elemento fuego = new Fuego();

		j1.crearMonstruo(fuego, fuego);
		Jugador j2 = new Jugador("Luis");

		Elemento agua = new Agua();
		j2.crearMonstruo(agua, agua);

		assertEquals(j1.miMonstruo.elemento1, fuego);
		assertEquals(j1.miMonstruo.elemento2, fuego);

		assertEquals(j2.miMonstruo.elemento1, agua);
		assertEquals(j2.miMonstruo.elemento2, agua);

		assertEquals(j1.obtenerNombre(), "Luis");

		assertEquals(j2.obtenerNombre(), "Luis");

		assertEquals(j1.obtenerVida(), 100);
		assertEquals(j2.obtenerVida(), 100);

	}

	@Test
	void TresJugadores() throws Exception {

		Jugador j1 = new Jugador("Luis");

		Elemento fuego = new Fuego();
		Elemento aire = new Aire();

		j1.crearMonstruo(fuego, aire);
		Jugador j2 = new Jugador("Gian");

		Elemento tierra = new Tierra();
		Elemento agua = new Agua();
		j2.crearMonstruo(agua, tierra);
		
		Jugador j3 = new Jugador("Mauro");

		j3.crearMonstruo(agua, fuego);
		
		assertEquals(j1.miMonstruo.elemento1, fuego);
		assertEquals(j1.miMonstruo.elemento2, aire);

		assertEquals(j2.miMonstruo.elemento1, agua);
		assertEquals(j2.miMonstruo.elemento2, tierra);

		assertEquals(j1.obtenerNombre(), "Luis");

		assertEquals(j2.obtenerNombre(), "Gian");

		assertEquals(j1.obtenerVida(), 100);
		assertEquals(j2.obtenerVida(), 100);
		
			
		
	}

}
