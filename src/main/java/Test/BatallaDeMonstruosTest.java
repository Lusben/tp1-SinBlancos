package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Assert;
import org.junit.internal.runners.ErrorReportingRunner;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import Elementos.Agua;
import Elementos.Aire;
import Elementos.Elemento;
import Elementos.Fuego;
import Elementos.Tierra;
import Juego.BatallaDeMonstruos;
import Juego.Jugador;


class BatallaDeMonstruosTest {

	@Test
	void GanoElJugadoUnoConAtaqueEspeciales() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		b.elegirElementoAire();
		b.elegirElementoFuego();

		b.crearMounstruos();

		b.elegirElementoAgua();
		b.elegirElementoTierra();

		b.crearMounstruos();

		b.ataque(2);// j1 ataco a j2
		b.ataque(2);// j2 ataco a j1

		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);

		assertEquals(b.j1.obtenerVida(), 10);

		assertEquals(b.j2.obtenerVida(), 0);
		assertEquals(b.hayGanador().obtenerNombre(), "Gian");

	}

	@Test
	void GanaElJugadorDos() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		b.elegirElementoAire();
		b.elegirElementoFuego();

		b.crearMounstruos();

		b.elegirElementoAgua();
		b.elegirElementoTierra();

		b.crearMounstruos();

		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		assertEquals(b.j1.obtenerVida(), 0);

		assertEquals(b.j2.obtenerVida(), 4);

		assertEquals(b.hayGanador().obtenerNombre(), "Luis");

	}

	@Test
	@DisplayName("The test that always fails")
	void NoSeCreaElMonstruoDelJugador2() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		b.elegirElementoAire();
		b.elegirElementoFuego();

		b.crearMounstruos();

	}

	@Test
	void seCreaLosJugadores1Y2() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		assertEquals(b.j1.obtenerNombre(), "Gian");

		assertEquals(b.j2.obtenerNombre(), "Luis");

	}

	@Test
	void seCreaLosJugadores1Y2YNoHayGanador() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");
		// jugador 1
		b.elegirElementoAgua();
		b.elegirElementoFuego();
		b.crearMounstruos();
		// jugador 2
		b.elegirElementoAire();
		b.elegirElementoFuego();
		b.crearMounstruos();

		b.ataque(2);

		b.ataque(3);

		b.ataque(5);

		b.ataque(6);

		b.ataque(2);

		b.ataque(7);
		
		assertEquals(b.j1.obtenerNombre(), "Gian");

		
		assertEquals(b.j2.obtenerNombre(), "Luis");

		assertEquals(b.hayUnGanador() , false );
	}
	
	@Test
	@DisplayName("The test that always fails")
	void noSeCreanElementos() throws Exception {
		
		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");
		
		b.crearMounstruos();
		
		
		b.crearMounstruos();
		
		
	}
	
	
}
