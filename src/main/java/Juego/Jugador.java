package Juego;

import Elementos.Elemento;
import Excepciones.ErrorDeAtaque;
import Excepciones.ErrorMounstruoCreado;

public class Jugador {
	public String nombre;
	public String tipoDeAtaque;
	public Elemento tipoDeElemento;
	public Elemento elemento1;
	public Elemento elemento2;
	public Monstruo miMonstruo;
	boolean esGanador = false;
	boolean ataco;
	String nombreDelMonstruo;
	Jugador JugadorEnemigo;

	int maxAtkEspeciales = 0;

	public Jugador(String nombre) {

		this.nombre = nombre;

	}

	public void crearMonstruo(Elemento e1, Elemento e2) throws Exception { // Crea un mounstruo, en el caso de
																			// que ya haya creado uno tira
																			// error.

		try {

			if (miMonstruo != null) {
				mounstruoCreado();

			} else {
				miMonstruo = new Monstruo(e1, e2);
				elemento1 = miMonstruo.elemento1;
				elemento2 = miMonstruo.elemento2;
			}
		}

		catch (ErrorMounstruoCreado e) {
			System.out.println(e);
		}

	}

	public double obtenerVida() {
		double vida = miMonstruo.mostrarVida();
		return vida;

	}

	public void elegirAtaque(int nroDelAtaque) throws Exception {
		try {
			if (nroDelAtaque < 0 || nroDelAtaque > 4) {

				errorDeAtaque();
			}

			else {

				if (nroDelAtaque == 1) {

					tipoDeAtaque = "Normal";
					tipoDeElemento = elemento1;

				}
				if (nroDelAtaque == 2 && maxAtkEspeciales < 4) {

					tipoDeAtaque = "Especial";
					tipoDeElemento = elemento1;
					maxAtkEspeciales++;

				}

				if (elemento2 != null && nroDelAtaque == 3) {

					tipoDeAtaque = "Normal";
					tipoDeElemento = elemento2;
				}
				if (elemento2 != null && nroDelAtaque == 4 && maxAtkEspeciales < 4) {

					tipoDeAtaque = "Especial";
					tipoDeElemento = elemento2;
					maxAtkEspeciales++;
				}

				ataco = true;
			}
		} catch (ErrorDeAtaque e) {
			System.out.println(e);
		}

	}

	public boolean esGanador() {

		return esGanador = true;
	}

	public String obtenerNombre() {
		return nombre;
	}

	

	Monstruo mostrarMonstruo() {

		return miMonstruo;
	}

	private void mounstruoCreado() throws Exception {
		throw new ErrorMounstruoCreado();

	}

	public void errorDeAtaque() throws Exception {
		throw new ErrorDeAtaque();

	}

}