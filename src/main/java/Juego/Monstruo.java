package Juego;

import Elementos.Elemento;

public class Monstruo {

	public Elemento elemento1;
	public Elemento elemento2;
	public double vida = 100;
	public Jugador quienMeAtaca;
	int nroDeAtaquesDisponibles;
	int ataque = 0;
	

	public Monstruo(Elemento e1, Elemento e2  ) {

		elemento1 = e1;
		elemento2 = e2;
		

		
	}


	public void meAtacan(Jugador quienMeAtaca) {
		this.quienMeAtaca = quienMeAtaca;
		if (quienMeAtaca.ataco == true) {
			String elementoAtacante = quienMeAtaca.tipoDeElemento.nombre() ;
			String debilidad = elemento1.debilidad();
			String debilidad2 = elemento2.debilidad();
			String fortaleza1 = elemento1.fuerteA();
			String fortaleza2 = elemento2.fuerteA();
			
			if (elementoAtacante == debilidad
					|| elementoAtacante == debilidad2) {            // Verifico que el elemento es el que me causa mas da�o
				if (quienMeAtaca.tipoDeAtaque == "Normal") {
					vida = vida - 12;   
					ataque = 12;//Se resta el ataque normal mas el %20
				}
				if (quienMeAtaca.tipoDeAtaque == "Especial") {
					vida = vida - 18;  
					ataque= 18;//Se resta el ataque especial mas el %20
				}
			} 
			else if (elementoAtacante == fortaleza1
					|| elementoAtacante == fortaleza2) {           // Verifico que el elemento es el que me causa menos da�o

				if (quienMeAtaca.tipoDeAtaque == "Normal") {
					vida = vida - 8; 
					ataque= 8;//Se resta el ataque normal menos el %20
				}
				if (quienMeAtaca.tipoDeAtaque == "Especial") {
					vida =  vida - 13; 
					ataque= 13;//Se Resta el ataque especial menos el %20
				}
			}  
			else if (quienMeAtaca.tipoDeAtaque == "Normal") {

				vida = vida -10;
				ataque= 10;

			} else if (quienMeAtaca.tipoDeAtaque == "Especial") {

				vida = vida- 15;
				ataque= 15;
			}

		}
		quienMeAtaca.ataco = false;

	}
	
	

	public double mostrarVida() {
		if(vida < 0) {
			vida = 0;
		}
		return vida;

	}

}
